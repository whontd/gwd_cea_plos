######################### housekeeping ################################
# Date: 15 September 2017
# By: Christopher Fitzpatrick, World Health Organization
# Description: Cost description of the global guinea worm eradication programme to 2015
# Note: this simplified file is set up to run a limited number of iterations (e.g. 200); to get 1000 iterations, this file was run 5 times and results were appended.
# Terms of use: please write Christopher at fitzpatrickc@who.int
# R version: 3.0.2

######################### load packages #####################################
library(ggplot2)
library(dplyr)
library(magrittr)
library(RJSONIO)
library(stringr)
library(WDI)
library(mc2d)
library(reshape2)

# set uncertainty parameters
ndunc<-200 
set.seed(10)

# load data

# main dataset
data_main<-read.csv(file="data/data_main.csv", header=TRUE, skip=1, sep = ",", stringsAsFactors=FALSE)

# pre-2008 WHO financial
who_fin_2008<-read.csv(file="data/who_fin_2008.csv", header=TRUE, stringsAsFactors=FALSE)
who_fin_2008<-melt(who_fin_2008[,c("Country","Specified","X2000","X2001","X2002","X2003","X2004","X2005","X2006","X2007")])
who_fin_2008$year<-as.integer(substring(who_fin_2008$variable,first = 2))
who_fin_2008$fin_2008_who<-who_fin_2008$value
unique(who_fin_2008$Country)
# note not all are countries
# remove non-African countries (African continent not WHO region)
who_fin_2008<-subset(who_fin_2008, Country!="YEM")
## aggregated
who_fin_2008<-who_fin_2008 %>%
  group_by(Country, year) %>%
  summarize(fin_2008_who = sum(fin_2008_who, na.rm = TRUE))

# post-2008 WHO financial
who_fin<-read.csv(file="D:/Dropbox/yaws/data/who_fin.csv", header=TRUE, stringsAsFactors=FALSE)
unique(who_fin$ISO3)
# note not all are countries
# remove non-African countries
who_fin<-subset(who_fin, ISO3!="YEM")
## aggregated
who_fin_a<-who_fin %>%
  group_by(ISO3, year) %>%
  summarize(fin_who = sum(Amount, na.rm = TRUE))

# population (with projections) in thousands
pop<-read.csv(file="D:/Dropbox/yaws/data/total_population_1950_2030.csv", stringsAsFactors = FALSE, skip = 1, header=TRUE)
pop$pop<-as.integer(gsub(" ", "", pop$pop, fixed = TRUE))
pop$ISO[pop$ISO=="CAM"]<-"CMR"

# merge datasets
df<-full_join(data_main,who_fin_2008[,c("Country","year","fin_2008_who")],by=c("iso"="Country","yr"="year"))
df<-full_join(df,who_fin_a[,c("ISO3","year","fin_who")],by=c("iso"="ISO3","yr"="year"))
df<-left_join(df,pop[,c("ISO","yrs","pop")],by=c("iso"="ISO","yr"="yrs"))

# clean up some variables
df$pha[df$pha=="Precertification stage"]<-"Precertification"
df$pha[is.na(df$pha)]<-""

# remove
rm(data_main,pop,who_fin,who_fin_2008,who_fin_a)

# create variables of interest

# villages under surveillance
# VUS is the number of villages with cases or with beg/end endemicity status of 1 or 2
# we allow also as a minimum the number of villages reporting cases
df$vus<-pmax(df$vus_WHO,df$nvrc_GHO, na.rm=TRUE)

# total financial cost

# WHO
df$fin_who_tot[df$yr>=2000 & df$yr!=2008 & df$yr<=2014]<-rowSums(df[df$yr>=2000 & df$yr!=2008 & df$yr<=2014,c("fin_2008_who","fin_who")], na.rm=TRUE)
# we assume costs in the period 2009-2014 have been fully accounted for by existing datasets
# WHO costs in the period 2016-2020 are covered by fin_proj_CC_WHO below

# Carter Centre
df$fin_tcc_tot[df$yr>=2008 & df$yr<=2020]<-rowSums(df[df$yr>=2008 & df$yr<=2020,c("fin_CC","fin_proj_CC","fin_cap_CC","fin_cap_proj_CC")], na.rm=TRUE)
# we assume costs in the period 2008-2020 have been fully accounted for by existing datasets
# TCC costs in the period 2016-2020 are covered by fin_proj_CC_WHO below

# WHO + TCC
df$fin_tot<-rowSums(df[,c("fin_who_tot","fin_tcc_tot")], na.rm=FALSE)
df$fin_tot[df$yr>=2016]<-rowSums(df[df$yr>=2016,c("fin_tcc_tot","fin_proj_CC_WHO")], na.rm=TRUE)
# this total remains missing if either WHO or TCC costs are missing

############## Figure 2. Reported total financial costs by country phase and at global level, 2009-2014 ###########

#### plot total cost by phase and year
cost_ph_yr<-df %>%
  group_by(pha, yr) %>%
  summarize(fin_tot = sum(fin_tot, na.rm = FALSE))
cost_ph_yr<-melt(cost_ph_yr, id=c("pha","yr"))
unique(cost_ph_yr$pha)
cost_ph_yr$pha[cost_ph_yr$pha==""]<-"General"
cost_ph_yr$pha[cost_ph_yr$pha=="Certified free"]<-"Certification"
cost_ph_yr$pha[cost_ph_yr$pha=="Endemic"]<-"Implementation"
cost_ph_yr<-rename(cost_ph_yr, Phase=pha)

ggplot() + geom_line(data=cost_ph_yr[cost_ph_yr$yr>2008 & cost_ph_yr$yr<2015,], aes(x=yr, y=value/1000, color=Phase)) +
  scale_y_continuous(limits=c(), "nominal US$ (thousands)", expand = c(0, 0)) +
  scale_x_continuous(limits=c(), "Year", expand = c(0, 0))

ggsave("figs/fig2.tiff", scale=1, width=6, height=4, units="in",dpi = 300, compression="lzw")

######## Historic costs -----------

# costs during Endemic phase

## cost per vus among endemic countries
df$cost_pvus<-df$fin_tot/df$vus
df$cost_pvus[df$cost_pvus==Inf]<-NA
summary(df$cost_pvus[df$pha=="Endemic"], na.rm=TRUE)
quantile(df$cost_pvus[df$pha=="Endemic"], probs=c(0.025,0.5,0.975), na.rm=TRUE)

# cost per case among endemic countries
df$cost_pcase<-df$fin_tot/df$cases
df$cost_pcase[df$cost_pcase==Inf]<-NA
summary(df$cost_pcase[df$pha=="Endemic"], na.rm=TRUE)
quantile(df$cost_pcase[df$pha=="Endemic"], probs=c(0.025,0.5,0.975), na.rm=TRUE)

## cost per capita (using real data only) among endemic countries
df$cost_pc<-df$fin_tot/(df$pop*1000)
df$cost_pc[df$yr>=2016]<-NA
summary(df$cost_pc[df$pha=="Endemic"], na.rm=TRUE)
quantile(df$cost_pc[df$pha=="Endemic"], probs=c(0.025,0.5,0.975), na.rm=TRUE)
### excluding South Sudan
quantile(df$cost_pc[df$pha=="Endemic" & df$cnt!="South Sudan"], probs=c(0.025,0.5,0.975), na.rm=TRUE)
temp<-quantile(df$cost_pc[df$pha=="Endemic" & df$cnt!="South Sudan"], probs=c(0.025,0.5,0.975), na.rm=TRUE)
cost_pc_end<-rtriang(ndunc,min=temp[1],mode=temp[2],max=temp[3])
temp<-quantile(df$cost_pc[df$pha=="Endemic" & df$cnt=="South Sudan"], probs=c(0.025,0.5,0.975), na.rm=TRUE)
cost_pc_end_SUD<-rtriang(ndunc,min=temp[1],mode=temp[2],max=temp[3])
#### excluding South Sudan and zeros
quantile(df$cost_pc[df$pha=="Endemic" & df$cnt!="South Sudan" & df$cost_pc>0], probs=c(0.025,0.5,0.975), na.rm=TRUE)

# costs during pre-certification phase

## cost per capita among pre-certification countries with no villages reporting cases
summary(df$cost_pc[df$pha=="Precertification"])
quantile(df$cost_pc[df$pha=="Precertification"], probs=c(0.025,0.5,0.975), na.rm=TRUE)
temp<-quantile(df$cost_pc[df$pha=="Precertification"], probs=c(0.025,0.5,0.975), na.rm=TRUE)
cost_pc_pre<-runif(ndunc,min=temp[1],max=temp[2])
### excluding zeroes
quantile(df$cost_pc[df$pha=="Precertification" & df$cost_pc>0], probs=c(0.025,0.5,0.975), na.rm=TRUE)
temp<-quantile(df$cost_pc[df$pha=="Precertification" & df$cost_pc>0], probs=c(0.025,0.5,0.975), na.rm=TRUE)
cost_pc_pre_pos<-rtriang(ndunc,min=temp[1],mode=temp[2],max=temp[3])

# costs during certification phase

## cost per capita among certified countries (no villages reporting cases)
summary(df$cost_pc[df$pha=="Certified free"])
quantile(df$cost_pc[df$pha=="Certified free"], probs=c(0.025,0.5,0.975), na.rm=TRUE)
temp<-quantile(df$cost_pc[df$pha=="Certified free"], probs=c(0.025,0.5,0.975), na.rm=TRUE)
cost_pc_cer<-runif(ndunc,min=temp[1],max=temp[2])
# excluding zeroes
quantile(df$cost_pc[df$pha=="Certified free" & df$cost_pc>0], probs=c(0.025,0.5,0.975), na.rm=TRUE)

# WHO regional / global costs (no phase) per capita in pre-certification and certification countries
df_glo_fin<-df[df$pha=="",] %>%
  group_by(yr) %>%
  summarize(fin_tot = sum(fin_tot, na.rm = FALSE), fin_who_tot= sum(fin_who_tot, na.rm = FALSE), fin_tcc_tot=sum(fin_tcc_tot, na.rm = FALSE))
df_glo_pop<-df[df$pha=="Endemic" | df$pha=="Precertification" | df$pha=="Certified free",] %>%
  group_by(yr) %>%
  summarize(pop = sum(pop, na.rm = FALSE))
df_glo<-left_join(df_glo_pop,df_glo_fin)
df_glo$cost_pc<-df_glo$fin_tot/(df_glo$pop*1000)
summary(df_glo$cost_pc)
quantile(df_glo$cost_pc, probs=c(0.025,0.5,0.975), na.rm=TRUE)
temp<-quantile(df_glo$cost_pc, probs=c(0.025,0.5,0.975), na.rm=TRUE)
cost_pc_glo<-rtriang(ndunc,min=temp[1],mode=temp[2],max=temp[3])
# excluding zeroes
quantile(df_glo$cost_pc[df_glo$cost_pc>0], probs=c(0.025,0.5,0.975), na.rm=TRUE)

######### Figure 3. Reported costs per capita, by country, 2009-2014---------

df_s<-df[df$yr>2008 & df$yr<2015,c("cnt","yr","pha","cost_pc","cost_pvus","cost_pcase")]
df_s<-melt(df_s, id.vars = c("cnt","yr","pha"))

df_s$pha[df_s$pha=="Certified free"]<-"Certification"
df_s$pha[df_s$pha=="Endemic"]<-"Implementation"
df_s<-rename(df_s, Country=cnt)

ggplot() + geom_line(data=df_s[df_s$pha!="" & df_s$variable=="cost_pc",], aes(x=yr, y=value, color=Country)) +
  facet_wrap(~pha, scales="free_y", ncol=1) +
  scale_y_continuous(limits=c(), "nominal US$", expand = c(0, 0)) +
  scale_x_continuous(limits=c(), "Year", expand = c(0, 0))

df_s$Phase<-df_s$pha

ggplot() + geom_line(data=df_s[df_s$pha!="" & df_s$variable=="cost_pc",], aes(x=yr, y=value, color=Phase)) +
  facet_wrap(~Country, ncol=3) +
  scale_y_continuous(limits=c(), "nominal US$ per capita (log scale)", expand = c(0, 0),trans = "log10") +
  scale_x_continuous(limits=c(), "Year", expand = c(0, 0)) + 
  theme(axis.text.x=element_text(angle=90))

ggsave("figs/fig3.tiff", scale=1, width=7.5, height=8, units="in",dpi = 300, compression="lzw")

######### Projected costs during Endemic phase----------

# reshape dataframes for use in PSA

# pop
pop<-rbind(df[df$pha!="" & df$yr>=1986,c("iso","yr","pop")],cbind(iso="GLO",df_glo_pop[df_glo_pop$yr>=1986,]))
pop<-dcast(pop, iso~yr, sum)

# pha
temp<-as.data.frame(cbind(iso="GLO",yr=seq(1986,2020,1),pha=""))
temp$yr<-as.numeric(as.character(temp$yr))
temp$pha<-as.character(temp$pha)
pha<-rbind(df[df$pha!="" & df$yr>=1986,c("iso","yr","pha")],temp)
pha<-dcast(pha, iso~yr, max)
pha[pha==""]<-"NA"

# fin_tot
fin_tot<-rbind(df[df$pha!="" & df$yr>=1986,c("iso","yr","fin_tot")],cbind(iso="GLO",df_glo[df_glo$yr>=1986,c("yr","fin_tot")]))
fin_tot<-dcast(fin_tot, iso~yr, sum)
# add World Bank data in nominal US$ (Other sources)
fin_tot$`1986`[fin_tot$iso=="GLO"]<-0
fin_tot$`1987`[fin_tot$iso=="GLO"]<-230000+1260000
fin_tot$`1988`[fin_tot$iso=="GLO"]<-260000+2810000
fin_tot$`1989`[fin_tot$iso=="GLO"]<-300000+3120000
fin_tot$`1990`[fin_tot$iso=="GLO"]<-2770000+3600000
fin_tot$`1991`[fin_tot$iso=="GLO"]<-2120000+5840000
fin_tot$`1992`[fin_tot$iso=="GLO"]<-2930000+2500000
fin_tot$`1993`[fin_tot$iso=="GLO"]<-8950000+7680000
fin_tot$`1994`[fin_tot$iso=="GLO"]<-3270000+6630000
fin_tot$`1995`[fin_tot$iso=="GLO"]<-3390000+8660000
fin_tot$`1996`[fin_tot$iso=="GLO"]<-4200000+6670000
fin_tot[fin_tot$iso!="GLO",c("1986","1987","1988","1989","1990","1991","1992","1993","1994","1995","1996")]<-0

# fin_who_tot
fin_who_tot<-rbind(df[df$pha!="" & df$yr>=1986,c("iso","yr","fin_who_tot")],cbind(iso="GLO",df_glo[df_glo$yr>=1986,c("yr","fin_who_tot")]))
fin_who_tot<-dcast(fin_who_tot, iso~yr, sum)
# use TCC data for WHO in 2008 and 2015
fin_who_tot$`2008`[fin_who_tot$iso=="GLO"]<-550000
fin_who_tot$`2015`[fin_who_tot$iso=="GLO"]<-6600000
fin_who_tot[fin_who_tot$iso!="GLO",c("2008","2015")]<-0

# fin_tcc_tot
fin_tcc_tot<-rbind(df[df$pha!="" & df$yr>=1986,c("iso","yr","fin_tcc_tot")],cbind(iso="GLO",yr=seq(1986,2020,1),fin_tcc_tot=0))
fin_tcc_tot$fin_tcc_tot<-as.numeric(fin_tcc_tot$fin_tcc_tot)
fin_tcc_tot<-dcast(fin_tcc_tot, iso~yr, sum)

# create uncertain array
iso<-c(pop$iso)
iso_num<-length(iso)

# adding also global to uncertain array
array3d<-array(0,dim=c(iso_num, 2020-1986+1, ndunc), dimnames=list(iso=iso,cycle=1986:2020,ndunc=1:ndunc))
array3d[1:10,1:10,1:10]

# we generate both WHO and TCC costs separately so that we can use WHO/TCC data in years where data exists for one both not both
fin_who_proj<-array3d
fin_tcc_proj<-array3d
fin_proj<-array3d

# populate uncertain array with estimates
for (i in 1:iso_num) {
  for (t in 1:(2020-1986+1)){
    if(pha[i,t+1]=="NA") {fin_who_proj[i,t,] <- pop[i,t+1]*1000*cost_pc_glo} else 
      if(pha[i,t+1]=="Endemic") {fin_tcc_proj[i,t,] <- pop[i,t+1]*1000*cost_pc_end} else
        if(pha[i,t+1]=="Precertification" & t>(2015-1986+1)) {fin_who_proj[i,t,] <- pop[i,t+1]*1000*cost_pc_pre_pos} else
          if(pha[i,t+1]=="Precertification") {fin_who_proj[i,t,] <- pop[i,t+1]*1000*cost_pc_pre} else
            if(pha[i,t+1]=="Certified free") {fin_who_proj[i,t,] <- pop[i,t+1]*1000*cost_pc_cer} 
    # and use Sudan costs for Sudan only
    if(i=="SDN" & pha[i,t+1]=="Endemic") {fin_tcc_proj[i,t,] <- pop[i,t+1]*1000*cost_pc_end_SUD}
    # but preserving true costs when we have them, such as 2009-2014 
    if(is.na(fin_who_tot[i,t+1])) print(t) else {fin_who_proj[i,t,] <- fin_who_tot[i,t+1]}
    if(is.na(fin_tcc_tot[i,t+1])) print(t) else {fin_tcc_proj[i,t,] <- fin_tcc_tot[i,t+1]}
    fin_proj[i,t,]<-pmax(fin_who_proj[i,t,],0,na.rm=TRUE)+pmax(fin_tcc_proj[i,t,],0,na.rm=TRUE)
    if(is.na(fin_tot[i,t+1])) print(t) else {fin_proj[i,t,] <- fin_tot[i,t+1]}
  }
}

fin_proj[,1:35,1]

temp<-as.data.frame(apply(fin_who_proj, c(2,3), sum))

summarize(fin_who_tot$`2008`)

############## Figure 4. Estimated total financial costs of the GWEP, 1986-2020 ###########

# plot total costs over time (with uncertainty ranges)
fin_tot_glo_yr<-as.data.frame(apply(fin_proj, c(2,3), sum))

# we take means and quantiles in one function
multi.fun <- function(x) {
  c(mean = mean(x), lo = quantile(x, probs=c(0.025), na.rm=TRUE), hi = quantile(x, probs=c(0.975), na.rm=TRUE))
}
fin_tot_glo_yr_q<-apply(fin_tot_glo_yr,1,function(x) multi.fun(x))
fin_tot_glo_yr_q<-as.data.frame(cbind(year=rownames(t(fin_tot_glo_yr_q)),t(fin_tot_glo_yr_q)), stringsAsFactors = FALSE)
names(fin_tot_glo_yr_q)<-c("year","best","lo","hi")
fin_tot_glo_yr_q$year<-as.numeric(fin_tot_glo_yr_q$year)
fin_tot_glo_yr_q$best<-as.numeric(fin_tot_glo_yr_q$best)
fin_tot_glo_yr_q$lo<-as.numeric(fin_tot_glo_yr_q$lo)
fin_tot_glo_yr_q$hi<-as.numeric(fin_tot_glo_yr_q$hi)
# plot
p<-ggplot(data=fin_tot_glo_yr_q) 
p + geom_line(aes(x=year,y=best/1000000)) + geom_ribbon(aes(x=year, ymin=lo/1000000, ymax=hi/1000000), alpha=0.5) + 
  expand_limits(y = 0) + 
  scale_y_continuous(limits=c(), "nominal US$ (millions)", expand = c(0, 0))+
  scale_x_continuous(limits=c(1986, 2020), "Year", expand = c(0, 0))

############# COST OF CONTROL ##############

# Maintaining implementation costs in endemic countries and pre-certification costs in neighbouring countries, rather than pursuing certification of eradication
temp<-as.data.frame(cbind(iso="GLO",yr=seq(1986,2020,1),pha=""))
temp$yr<-as.numeric(as.character(temp$yr))
temp$pha<-as.character(temp$pha)
pha<-rbind(df[df$pha!="" & df$yr>=1986,c("iso","yr","pha")],temp)
pha_2015<-pha[pha$yr==2015,]
names(pha_2015)<-c("iso","yr","pha_2015")
pha<-left_join(pha,pha_2015[,c("iso","pha_2015")])
pha$pha[pha$yr>2015 & pha$pha_2015!="Certified free"]<-"Precertification"
pha<-subset(pha,select=-pha_2015)
# pha$pha[pha$yr>2015 & pha$iso!="GLO"]<-"Precertification"
pha<-dcast(pha, iso~yr, max)
pha[pha==""]<-"NA"

# remove projected fin_tcc_tot and global tot
fin_tcc_tot[,c("2016","2017","2018","2019","2020")]<-NA
fin_tot[,c("2016","2017","2018","2019","2020")]<-NA
# !!! except that fin_tot is 0 for GLO (no global costs in control scenario)
#fin_tot[fin_tot$iso=="GLO",c("2016","2017","2018","2019","2020")]<-0

array3d<-array(0,dim=c(iso_num, 2020-1986+1, ndunc), dimnames=list(iso=iso,cycle=1986:2020,ndunc=1:ndunc))
fin_who_proj<-array3d
fin_tcc_proj<-array3d
fin_proj_alt<-array3d

for (i in 1:iso_num) {
  for (t in 1:(2020-1986+1)){
    if(pha[i,t+1]=="NA") {fin_who_proj[i,t,] <- pop[i,t+1]*1000*cost_pc_glo} else 
      if(pha[i,t+1]=="NA") {fin_who_proj[i,t,] <- pop[i,t+1]*1000*cost_pc_glo} else 
        if(pha[i,t+1]=="Endemic") {fin_tcc_proj[i,t,] <- pop[i,t+1]*1000*cost_pc_end} else
          if(pha[i,t+1]=="Precertification" & t>(2015-1986+1)) {fin_who_proj[i,t,] <- pop[i,t+1]*1000*cost_pc_pre_pos} else
            if(pha[i,t+1]=="Precertification") {fin_who_proj[i,t,] <- pop[i,t+1]*1000*cost_pc_pre} else
              if(pha[i,t+1]=="Certified free") {fin_who_proj[i,t,] <- pop[i,t+1]*1000*cost_pc_cer} 
    # and use Sudan costs for Sudan only during endemic phase
    if(i=="SDN" & pha[i,t+1]=="Endemic") {fin_tcc_proj[i,t,] <- pop[i,t+1]*1000*cost_pc_end_SUD}
    # but preserving true costs when we have them, such as 2009-2014
    if(is.na(fin_who_tot[i,t+1])) print(t) else {fin_who_proj[i,t,] <- fin_who_tot[i,t+1]}
    if(is.na(fin_tcc_tot[i,t+1])) print(t) else {fin_tcc_proj[i,t,] <- fin_tcc_tot[i,t+1]}
    fin_proj_alt[i,t,]<-pmax(fin_who_proj[i,t,],0,na.rm=TRUE)+pmax(fin_tcc_proj[i,t,],0,na.rm=TRUE)
    if(is.na(fin_tot[i,t+1])) print(t) else {fin_proj_alt[i,t,] <- fin_tot[i,t+1]}
  }
}

# plot total costs over time (with uncertainty ranges)
fin_tot_glo_yr_alt<-as.data.frame(apply(fin_proj_alt, c(2,3), sum))

# we take means and quantiles in one function
multi.fun <- function(x) {
  c(mean = mean(x), lo = quantile(x, probs=c(0.025), na.rm=TRUE), hi = quantile(x, probs=c(0.975), na.rm=TRUE))
}
fin_tot_glo_yr_q_alt<-apply(fin_tot_glo_yr_alt,1,function(x) multi.fun(x))
fin_tot_glo_yr_q_alt<-as.data.frame(cbind(year=rownames(t(fin_tot_glo_yr_q_alt)),t(fin_tot_glo_yr_q_alt)), stringsAsFactors = FALSE)
names(fin_tot_glo_yr_q_alt)<-c("year","best","lo","hi")
fin_tot_glo_yr_q_alt$year<-as.numeric(fin_tot_glo_yr_q_alt$year)
fin_tot_glo_yr_q_alt$best<-as.numeric(fin_tot_glo_yr_q_alt$best)
fin_tot_glo_yr_q_alt$lo<-as.numeric(fin_tot_glo_yr_q_alt$lo)
fin_tot_glo_yr_q_alt$hi<-as.numeric(fin_tot_glo_yr_q_alt$hi)

# plot
p<-ggplot(data=fin_tot_glo_yr_q_alt) 
p + geom_line(aes(x=year,y=best/1000000)) + geom_ribbon(aes(x=year, ymin=lo/1000000, ymax=hi/1000000), alpha=0.5) + 
  expand_limits(y = 0) + 
  scale_y_continuous(limits=c(), "nominal US$ (millions)", expand = c(0, 0))+
  scale_x_continuous(limits=c(1986, 2020), "Year", expand = c(0, 0))

### COMBINED PLOTS #####

fin_tot_glo_yr_q$Scenario<-"GWEP"
fin_tot_glo_yr_q_alt$Scenario<-"Control"

temp<-rbind(fin_tot_glo_yr_q,fin_tot_glo_yr_q_alt)

# plot
p<-ggplot(data=temp) 
p + geom_line(aes(x=year,y=best/1000000,color=Scenario)) + geom_ribbon(aes(x=year, ymin=lo/1000000, ymax=hi/1000000,color=Scenario), alpha=0.5) + 
  expand_limits(y = 0) + 
  scale_y_continuous(limits=c(), "nominal US$ (millions)", expand = c(0, 0))+
  scale_x_continuous(limits=c(1986, 2020), "Year", expand = c(0, 0))

ggsave("figs/fig4.tiff", scale=1, width=6, height=4, units="in",dpi = 300, compression="lzw")

########## nominal undiscounted costs #############
# this is what can be compared to TCC estimate
# GWEP
fin_tot_glo<-apply(fin_proj, c(3), sum)
mean(fin_tot_glo)
quantile(fin_tot_glo,c(0.025,0.975))
# GWEP in 1986-2014
temp<-apply(fin_proj[,1:29,], c(3), sum)
mean(temp)
quantile(temp,c(0.025,0.975))
# GWEP in 1986-2004
temp<-apply(fin_proj[,1:19,], c(3), sum)
mean(temp)
quantile(temp,c(0.025,0.975))
# Control
fin_tot_glo_alt<-apply(fin_proj_alt, c(3), sum)
mean(fin_tot_glo_alt)
quantile(fin_tot_glo_alt,c(0.025,0.975))

########## constant costs ##################

# x<-WDIsearch(string = "deflator", field = "name", short = TRUE, cache = NULL)
# def<-WDI(country = "all", indicator = "NY.GDP.DEFL.KD.ZG", start = 1986, end = 2020, extra = TRUE, cache = NULL)
# save(def, file="data/def.Rdata")
load("data/def.Rdata")
def<-def[def$country=="United States",c("year","NY.GDP.DEFL.KD.ZG")]
names(def)<-c("yr","def")
def_m<-median(def$def, na.rm=TRUE)/100

# change to constant 2015 USD using US GDP deflator
for (i in 1:(2020-1986+1)) {
  fin_proj[,i,]<-fin_proj[,i,]/(1+def_m)^(i-(2015-1986+1))
  fin_proj_alt[,i,]<-fin_proj_alt[,i,]/(1+def_m)^(i-(2015-1986+1))
  }

mean(apply(fin_proj, c(3), sum))

##########  discounted constant costs ##############

# discount rate on costs (country but not year specific)
temp<-array(exp(log(1+runif(ndunc,min=0.00, max=0.03))*1)-1,dim=c(iso_num, ndunc))
disc_c<-array3d
for (i in 1:iso_num) {
  for (j in 1:ndunc){
    disc_c[i,,j]<-temp[i,j]
  }
}

# discounted costs from perspective of 1986
# yr_zero<-2015-1986+1
for (i in 1:(2020-1986+1)) {
  fin_proj[,i,]<-fin_proj[,i,]/(1+disc_c[,i,])^(i-1)
  fin_proj_alt[,i,]<-fin_proj_alt[,i,]/(1+disc_c[,i,])^(i-1)
}

# total discounted costs (global and period total) 
# these are the vectors that go into the BCEA
# GWEP
tot_fin_glo<-apply(fin_proj, c(3), sum)
summary(tot_fin_glo)
tot_fin_glo_96<-apply(fin_proj[,1:(1996-1986+1),], c(3), sum)
save(tot_fin_glo, tot_fin_glo_96, file="data/tot_fin_glo.Rdata")
# Control
tot_fin_glo_alt<-apply(fin_proj_alt, c(3), sum)
summary(tot_fin_glo_alt)
tot_fin_glo_alt_96<-apply(fin_proj_alt[,1:(1996-1986+1),], c(3), sum)
save(tot_fin_glo_alt, tot_fin_glo_alt_96, file="data/tot_fin_glo_alt.Rdata")



